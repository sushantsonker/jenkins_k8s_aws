provider "aws" {
  region = "us-east-1"
}

##################################################################
# Data sources to get VPC, subnet, security group and AMI details
##################################################################
data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = "${data.aws_vpc.default.id}"
}

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]
 

  filter {
    name = "image-id"

    values = [
      "ami-0de53d8956e8dcf80",
    ]
  }
}

data "aws_ami" "amazon_coreos" {
  most_recent = true
  owners      = ["595879546273"]

  filter {
    name = "image-id"

    values = [
      "ami-0089347d530e1f3e6",
    ]
  }
}

data "aws_security_groups" "sg" {
  filter {
    name   = "group-name"
    values = ["default"]
  }

  filter {
    name   = "vpc-id"
    values = ["${data.aws_vpc.default.id}"]
  }
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.amazon_coreos.id}"
  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld"
  }
}


module "jenkins_coreos" {
  
  source = "terraform-aws-modules/ec2-instance/aws"

  instance_count = 1

  name                        = "jenkins_coreos"
  ami                         = "${data.aws_ami.amazon_coreos.id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${element(data.aws_subnet_ids.all.ids, 0)}"
  vpc_security_group_ids      = ["${element(data.aws_security_groups.sg.ids, 0)}"]
  associate_public_ip_address = true
  key_name = "jenkins"

  root_block_device = [{
    volume_type = "gp2"
    volume_size = 10
  }]


  user_data = <<-EOF
                #!/bin/bash
                value=$( docker inspect -f '{{.State.Running}}' jenkins )
                echo $value
                if [ $value ]
                then
                  echo "jenkins docker container is running"
                else
                  docker run -p 8080:8080 -p 50000:50000 jenkins/jenkins:lts
                fi
                sleep 60
                sudo mkdir /var/jenkins
                sudo chmod 777 /var/jenkins
                sleep 5
                docker start $(docker ps -q)
                EOF
}

module "Kube-aws-amazon_linux" {

  source = "terraform-aws-modules/ec2-instance/aws"  

  instance_count = 1

  name                        = "Kube-aws"
  ami                         = "${data.aws_ami.amazon_linux.id}"
  instance_type               = "t2.micro"
  cpu_credits                 = "unlimited"
  subnet_id                   = "${element(data.aws_subnet_ids.all.ids, 0)}"
  vpc_security_group_ids      = ["${element(data.aws_security_groups.sg.ids, 0)}"]
  associate_public_ip_address = true
  key_name = "jenkins"

    user_data = <<-EOF
                #!/bin/bash
                sudo mkdir -p /var/jenkins
                sudo chmod 777 /var/jenkins
                yum -y install java
                sleep 30
                EOF
}

